import orjson as json

import click

from .monitor import Monitor


@click.command()
@click.argument("data-file", type=click.File("r"))
@click.option("-t", "--alert-threshold", type=int, default=3, show_default=True)
@click.option("-w", "--alert-window", type=int, default=5, show_default=True)
@click.option(
    "-o", "--output-format", type=click.Choice(["json", "text"]), default="json",
    show_default=True,
)
def main(data_file, alert_threshold, alert_window, output_format):
    monitor = Monitor.from_raw_data(
        data_file.read(),
        alert_threshold=alert_threshold,
        alert_window=alert_window,
    )
    alerts = monitor.get_alerts()
    if output_format == "json":
        click.echo(json.dumps(alerts, option=json.OPT_INDENT_2))
    else:
        for alert in alerts:
            click.echo(
                f'{alert["timestamp"]} - {alert["component"]} - {alert["satelliteId"]}'
            )
