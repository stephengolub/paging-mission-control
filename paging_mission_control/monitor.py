from __future__ import annotations

from collections import defaultdict
from datetime import datetime, timedelta
import typing as t

from .telemetry_data import TelemetryDataPoint


class Monitor:
    """Represents a monitor that can emits alerts from telemetry data.

    Args:
        data_points: A list of telemetry data points.
        alert_threshold: The number of data points that must be in an alert state
        alert_window: The time window in which the alert threshold must be met.

    .. note:: The alert window is inclusive of the start and end times.
    """

    def __init__(
        self,
        data_points: list[TelemetryDataPoint],
        alert_threshold: int = 3,
        alert_window: t.Union[timedelta, int] = timedelta(minutes=5),
    ):
        self.sat_datapoints = defaultdict(lambda: defaultdict(list))
        self.alert_threshold = alert_threshold
        if not isinstance(alert_window, timedelta):
            alert_window = timedelta(minutes=alert_window)
        self.alert_window = alert_window

        for data_point in data_points:
            self.sat_datapoints[data_point.satellite_id][data_point.component].append(
                data_point
            )

        # Ensure that the data points are sorted by timestamp
        for components in self.sat_datapoints.values():
            for data_points in components.values():
                data_points.sort(key=lambda dp: dp.timestamp)

    @classmethod
    def from_raw_data(cls, raw_data: str, **kwargs) -> Monitor:
        """Create a monitor from raw telemetry data.

        Args:
            raw_data: The raw telemetry data. Each line should be a single telemetry data point.
            kwargs: Additional keyword arguments to pass to the Monitor constructor.

        Returns:
            A monitor instance.
        """
        return cls(
            [TelemetryDataPoint(line) for line in raw_data.splitlines()],
            **kwargs,
        )

    def get_alerts(self) -> list[dict[str, t.Any]]:
        """Get a list of alerts from the telemetry data.

        Returns:
            A list of alerts. Each alert is pulled from the :attr:`TelemetryDataPoint.alert` property.
        """
        alerts = []
        for sat_id, sat_map in self.sat_datapoints.items():
            for component, data_points in sat_map.items():
                alert_dps = []
                for dp in data_points:
                    if dp.alert is not None:
                        alert_dps.append(dp)
                    if (
                        alert_dps
                        and alert_dps[-1].timestamp - alert_dps[0].timestamp
                        > self.alert_window
                    ):
                        # Out of the alert window, so we can stop checking it
                        alert_dps.pop(0)
                    if len(alert_dps) >= self.alert_threshold:
                        alerts.append(alert_dps[0])
                        break
        return [a.alert for a in alerts]
