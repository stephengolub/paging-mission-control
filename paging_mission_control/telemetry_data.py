from __future__ import annotations

from collections import defaultdict
from datetime import datetime
from functools import cached_property
import typing as t


class TelemetryDataPoint:
    """Represents a single data point from the telemetry.

    Args:
        input_data: The raw input data from the telemetry. The format is
            expected to be a pipe-separated list of values:
            timestamp|satellite_id|red_high_limit|yellow_high_limit|yellow_low_limit|red_low_limit|raw_value|component
    """

    input_map: dict[str, t.Optional[t.Callable[[str], t.Any]]] = {
        "timestamp": lambda dt_str: datetime.strptime(
            f"{dt_str}000", "%Y%m%d %H:%M:%S.%f"
        ),
        "satellite_id": int,
        "red_high_limit": int,
        "yellow_high_limit": int,
        "yellow_low_limit": int,
        "red_low_limit": int,
        "raw_value": float,
        "component": None,
    }
    """A mapping for data points and how to convert them.

    The keys are the names of the data points, and the values should be callables
    to convert the data point to the desired type. If the value is None, then
    the data point should be left as a string.

    .. note:: Since the release of Python 3.7, dictionaries are guaranteed to preserve order.
        This means that we can use a dictionary to map the input data to the correct
        attribute names and types.
    """

    def __init__(self, input_data: str):
        self._raw_input = input_data

    @cached_property
    def raw_input(self) -> dict[str, t.Any]:
        """The raw input data from the telemetry.

        Returns:
            A dictionary of the raw input data. The keys are the names of the data points,
            and the values are the values from the telemetry input after being converted
            to the correct type defined in :attr:`input_map`.
        """
        result = {}
        data = self._raw_input.split("|")
        for key, parser in self.input_map.items():
            try:
                result[key] = data.pop(0)
            except IndexError:
                raise TypeError(f"Invalid input data: {self._raw_input}")
            else:
                if parser:
                    try:
                        result[key] = parser(result[key])
                    except ValueError:
                        raise TypeError(f"Invalid input data: {self._raw_input}")
        return result

    def __getitem__(self, key):
        return self.raw_input[key]

    def __getattr__(self, key):
        return self.raw_input[key]

    @cached_property
    def alert_ranges(self) -> dict[str, tuple[int, int]]:
        """The alert ranges for the telemetry data point.

        Returns:
            A dictionary with the keys ``red`` and ``yellow``. The values are
            tuples of the low and high values for the alert range.
        """
        alert_ranges = defaultdict(lambda: [0, 0])
        for key, value in self.raw_input.items():
            if key.endswith("limit"):
                alert_type, alert_bound, _ = key.split("_")
                if alert_bound == "low":
                    alert_ranges[alert_type][0] = value
                else:
                    alert_ranges[alert_type][1] = value
        return dict(alert_ranges)

    @cached_property
    def alert_type(self) -> t.Optional[str]:
        """The type of alert that this data point represents.

        Returns:
            The alert type, or None if the data point is not in an alert state.
        """
        for alert_type, (low, high) in self.alert_ranges.items():
            if low > self.raw_value or self.raw_value > high:
                return alert_type
        return None

    @cached_property
    def alert(self) -> t.Optional[dict]:
        """Get the alert, if any, data for this data point.

        Returns:
            A dictionary with the alert data, or None if there is no alert.
            The dictionary will have the following keys:
                - timestamp: The timestamp of the data point.
                - satelliteId: The ID of the satellite.
                - component: The component that triggered the alert.
                - severity: The severity of the alert.
        """
        if self.alert_type is not None:
            bounds = self.alert_ranges[self.alert_type]
            bound = ("low" if self["raw_value"] < bounds[0] else "high").upper()
            return {
                "satelliteId": self.satellite_id,
                "severity": f"{self.alert_type.upper()} {bound}",
                "component": self.component,
                "timestamp": self.timestamp,
            }
        return None
