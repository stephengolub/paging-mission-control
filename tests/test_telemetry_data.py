from datetime import datetime

import pytest

from paging_mission_control.telemetry_data import TelemetryDataPoint


class TestTelemetryDataPoint:
    @pytest.fixture(params=range(14), ids=lambda x: f'line {x+1}')
    def iteration(self, request):
        return request.param

    @pytest.fixture
    def input_lines(self, sample_data):
        return sample_data.splitlines()

    @pytest.fixture
    def input_line(self, input_lines, iteration):
        return input_lines[iteration]

    @pytest.fixture
    def expected_output_as_dict(self, iteration):
        return [
            {
                'timestamp': datetime(2018, 1, 1, 23, 1, 5, 1000),
                'satellite_id': 1001,
                'red_high_limit': 101,
                'yellow_high_limit': 98,
                'yellow_low_limit': 25,
                'red_low_limit': 20,
                'raw_value': 99.9,
                'component': 'TSTAT',
            },
            {
                'timestamp': datetime(2018, 1, 1, 23, 1, 9, 521000),
                'satellite_id': 1000,
                'red_high_limit': 17,
                'yellow_high_limit': 15,
                'yellow_low_limit': 9,
                'red_low_limit': 8,
                'raw_value': 7.8,
                'component': 'BATT',
            },
            {
                'timestamp': datetime(2018, 1, 1, 23, 1, 26, 11000),
                'satellite_id': 1001,
                'red_high_limit': 101,
                'yellow_high_limit': 98,
                'yellow_low_limit': 25,
                'red_low_limit': 20,
                'raw_value': 99.8,
                'component': 'TSTAT',
            },
            {
                'timestamp': datetime(2018, 1, 1, 23, 1, 38, 1000),
                'satellite_id': 1000,
                'red_high_limit': 101,
                'yellow_high_limit': 98,
                'yellow_low_limit': 25,
                'red_low_limit': 20,
                'raw_value': 102.9,
                'component': 'TSTAT',
            },
            {
                'timestamp': datetime(2018, 1, 1, 23, 1, 49, 21000),
                'satellite_id': 1000,
                'red_high_limit': 101,
                'yellow_high_limit': 98,
                'yellow_low_limit': 25,
                'red_low_limit': 20,
                'raw_value': 87.9,
                'component': 'TSTAT',
            },
            {
                'timestamp': datetime(2018, 1, 1, 23, 2, 9, 14000),
                'satellite_id': 1001,
                'red_high_limit': 101,
                'yellow_high_limit': 98,
                'yellow_low_limit': 25,
                'red_low_limit': 20,
                'raw_value': 89.3,
                'component': 'TSTAT',
            },
            {
                'timestamp': datetime(2018, 1, 1, 23, 2, 10, 21000),
                'satellite_id': 1001,
                'red_high_limit': 101,
                'yellow_high_limit': 98,
                'yellow_low_limit': 25,
                'red_low_limit': 20,
                'raw_value': 89.4,
                'component': 'TSTAT',
            },
            {
                'timestamp': datetime(2018, 1, 1, 23, 2, 11, 302000),
                'satellite_id': 1000,
                'red_high_limit': 17,
                'yellow_high_limit': 15,
                'yellow_low_limit': 9,
                'red_low_limit': 8,
                'raw_value': 7.7,
                'component': 'BATT',
            },
            {
                'timestamp': datetime(2018, 1, 1, 23, 3, 3, 8000),
                'satellite_id': 1000,
                'red_high_limit': 101,
                'yellow_high_limit': 98,
                'yellow_low_limit': 25,
                'red_low_limit': 20,
                'raw_value': 102.7,
                'component': 'TSTAT',
            },
            {
                'timestamp': datetime(2018, 1, 1, 23, 3, 5, 9000),
                'satellite_id': 1000,
                'red_high_limit': 101,
                'yellow_high_limit': 98,
                'yellow_low_limit': 25,
                'red_low_limit': 20,
                'raw_value': 101.2,
                'component': 'TSTAT',
            },
            {
                'timestamp': datetime(2018, 1, 1, 23, 4, 6, 17000),
                'satellite_id': 1001,
                'red_high_limit': 101,
                'yellow_high_limit': 98,
                'yellow_low_limit': 25,
                'red_low_limit': 20,
                'raw_value': 89.9,
                'component': 'TSTAT',
            },
            {
                'timestamp': datetime(2018, 1, 1, 23, 4, 11, 531000),
                'satellite_id': 1000,
                'red_high_limit': 17,
                'yellow_high_limit': 15,
                'yellow_low_limit': 9,
                'red_low_limit': 8,
                'raw_value': 7.9,
                'component': 'BATT',
            },
            {
                'timestamp': datetime(2018, 1, 1, 23, 5, 5, 21000),
                'satellite_id': 1001,
                'red_high_limit': 101,
                'yellow_high_limit': 98,
                'yellow_low_limit': 25,
                'red_low_limit': 20,
                'raw_value': 89.9,
                'component': 'TSTAT',
            },
            {
                'timestamp': datetime(2018, 1, 1, 23, 5, 7, 421000),
                'satellite_id': 1001,
                'red_high_limit': 17,
                'yellow_high_limit': 15,
                'yellow_low_limit': 9,
                'red_low_limit': 8,
                'raw_value': 7.9,
                'component': 'BATT',
            },
        ][iteration]

    @pytest.fixture
    def expected_alerts(self, iteration):
        return [
            {
                'timestamp': datetime(2018, 1, 1, 23, 1, 5, 1000),
                'satellite_id': 1001,
                'red_high_limit': 101,
                'yellow_high_limit': 98,
                'yellow_low_limit': 25,
                'red_low_limit': 20,
                'raw_value': 99.9,
                'component': 'TSTAT',
            },
            {
                'timestamp': datetime(2018, 1, 1, 23, 1, 9, 521000),
                'satellite_id': 1000,
                'red_high_limit': 17,
                'yellow_high_limit': 15,
                'yellow_low_limit': 9,
                'red_low_limit': 8,
                'raw_value': 7.8,
                'component': 'BATT',
            },
            {
                'timestamp': datetime(2018, 1, 1, 23, 1, 26, 11000),
                'satellite_id': 1001,
                'red_high_limit': 101,
                'yellow_high_limit': 98,
                'yellow_low_limit': 25,
                'red_low_limit': 20,
                'raw_value': 99.8,
                'component': 'TSTAT',
            },
            {
                'timestamp': datetime(2018, 1, 1, 23, 1, 38, 1000),
                'satellite_id': 1000,
                'red_high_limit': 101,
                'yellow_high_limit': 98,
                'yellow_low_limit': 25,
                'red_low_limit': 20,
                'raw_value': 102.9,
                'component': 'TSTAT',
            },
            {
                'timestamp': datetime(2018, 1, 1, 23, 1, 49, 21000),
                'satellite_id': 1000,
                'red_high_limit': 101,
                'yellow_high_limit': 98,
                'yellow_low_limit': 25,
                'red_low_limit': 20,
                'raw_value': 87.9,
                'component': 'TSTAT',
            },
            {
                'timestamp': datetime(2018, 1, 1, 23, 2, 9, 14000),
                'satellite_id': 1001,
                'red_high_limit': 101,
                'yellow_high_limit': 98,
                'yellow_low_limit': 25,
                'red_low_limit': 20,
                'raw_value': 89.3,
                'component': 'TSTAT',
            },
            {
                'timestamp': datetime(2018, 1, 1, 23, 2, 10, 21000),
                'satellite_id': 1001,
                'red_high_limit': 101,
                'yellow_high_limit': 98,
                'yellow_low_limit': 25,
                'red_low_limit': 20,
                'raw_value': 89.4,
                'component': 'TSTAT',
            },
            {
                'timestamp': datetime(2018, 1, 1, 23, 2, 11, 302000),
                'satellite_id': 1000,
                'red_high_limit': 17,
                'yellow_high_limit': 15,
                'yellow_low_limit': 9,
                'red_low_limit': 8,
                'raw_value': 7.7,
                'component': 'BATT',
            },
            {
                'timestamp': datetime(2018, 1, 1, 23, 3, 3, 8000),
                'satellite_id': 1000,
                'red_high_limit': 101,
                'yellow_high_limit': 98,
                'yellow_low_limit': 25,
                'red_low_limit': 20,
                'raw_value': 102.7,
                'component': 'TSTAT',
            },
            {
                'timestamp': datetime(2018, 1, 1, 23, 3, 5, 9000),
                'satellite_id': 1000,
                'red_high_limit': 101,
                'yellow_high_limit': 98,
                'yellow_low_limit': 25,
                'red_low_limit': 20,
                'raw_value': 101.2,
                'component': 'TSTAT',
            },
            {
                'timestamp': datetime(2018, 1, 1, 23, 4, 6, 17000),
                'satellite_id': 1001,
                'red_high_limit': 101,
                'yellow_high_limit': 98,
                'yellow_low_limit': 25,
                'red_low_limit': 20,
                'raw_value': 89.9,
                'component': 'TSTAT',
            },
            {
                'timestamp': datetime(2018, 1, 1, 23, 4, 11, 531000),
                'satellite_id': 1000,
                'red_high_limit': 17,
                'yellow_high_limit': 15,
                'yellow_low_limit': 9,
                'red_low_limit': 8,
                'raw_value': 7.9,
                'component': 'BATT',
            },
            {
                'timestamp': datetime(2018, 1, 1, 23, 5, 5, 21000),
                'satellite_id': 1001,
                'red_high_limit': 101,
                'yellow_high_limit': 98,
                'yellow_low_limit': 25,
                'red_low_limit': 20,
                'raw_value': 89.9,
                'component': 'TSTAT',
            },
            {
                'timestamp': datetime(2018, 1, 1, 23, 5, 7, 421000),
                'satellite_id': 1001,
                'red_high_limit': 17,
                'yellow_high_limit': 15,
                'yellow_low_limit': 9,
                'red_low_limit': 8,
                'raw_value': 7.9,
                'component': 'BATT',
            },
        ][iteration]

    def test_from_pdr(self, input_line, expected_output_as_dict):
        tdp = TelemetryDataPoint(input_line)
        assert tdp['timestamp'] == expected_output_as_dict['timestamp']
        assert tdp['satellite_id'] == expected_output_as_dict['satellite_id']
        assert tdp['red_high_limit'] == expected_output_as_dict['red_high_limit']
        assert tdp['yellow_high_limit'] == expected_output_as_dict['yellow_high_limit']
        assert tdp['yellow_low_limit'] == expected_output_as_dict['yellow_low_limit']
        assert tdp['red_low_limit'] == expected_output_as_dict['red_low_limit']
        assert tdp['raw_value'] == expected_output_as_dict['raw_value']
        assert tdp['component'] == expected_output_as_dict['component']

    def test_alerts(self, input_lines):
        tdps = [TelemetryDataPoint(line) for line in input_lines]
        alerts = []
        for tdp in tdps:
            alert = tdp.alert
            if alert:
                alerts.append(alert)
        assert len(alerts) == 9
