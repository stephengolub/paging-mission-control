from datetime import datetime, timedelta

import pytest

from paging_mission_control.monitor import Monitor
from paging_mission_control.telemetry_data import TelemetryDataPoint


class TestMonitor:
    @pytest.fixture
    def instance(self, sample_data):
        return Monitor.from_raw_data(sample_data)

    def test_init(self, instance):
        assert instance.alert_threshold == 3
        assert instance.alert_window == timedelta(minutes=5)

    def test_from_raw_data(self, instance):
        assert len(instance.sat_datapoints) == 2
        assert len(instance.sat_datapoints[1000]) == 2
        assert len(instance.sat_datapoints[1001]) == 2

    def test_get_alerts(self, instance):
        alerts = instance.get_alerts()
        assert len(alerts) == 2
        assert alerts == [
            {
                "satelliteId": 1000,
                "severity": "RED LOW",
                "component": "BATT",
                "timestamp": datetime(2018, 1, 1, 23, 1, 9, 521000),
            },
            {
                "satelliteId": 1000,
                "severity": "RED HIGH",
                "component": "TSTAT",
                "timestamp": datetime(2018, 1, 1, 23, 1, 38, 1000)
            }
        ]
