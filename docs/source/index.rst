.. Paging Mission Control documentation master file, created by
   sphinx-quickstart on Mon Feb 20 10:05:54 2023.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to Paging Mission Control's documentation!
==================================================

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   monitor.rst
   telemetry_data.rst

Setup
-----

First you'll need to install the ``poetry`` package:

.. code:: bash

   pipx install poetry

Then you can setup the project:

.. code:: bash

   poetry install

Console Script
--------------



.. runcmd:: poetry run pmc-analyze --help


Indices and tables
==================

* :ref:`genindex`
* :ref:`search`
